import React from 'react'
import { View, Text, ActivityIndicator, TouchableOpacity, FlatList, StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    item: {
        padding: 10
    },
    text: {
        alignItems: 'center',
        backgroundColor: '#FADDFF',
        paddingHorizontal: 10
    }
})

export default class Series extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            series: []
        }
    }

    componentDidMount() {
        return fetch('https://api.betaseries.com/shows/list?key=4ce0e9d6e598')
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    series: result.shows
                })
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                })
            }
        )
    }

    render() {
        if (this.state.isLoading) {
            return(
                <View>
                    <ActivityIndicator/>
                </View>
            )
        } else {
            const pressHandler = (id, title, seasons, episodes,length, description, genres) => {
            }
            return(
                <View>
                    <FlatList
                    data={this.state.series}
                    renderItem={({item}) => <TouchableOpacity style={styles.item} onPress={() => pressHandler(item.id, 
                    item.title, 
                    item.seasons, 
                    item.episodes, 
                    item.length, 
                    item.description,
                    item.genres)}>
                        <Text style={styles.text}>{item.title}</Text>
                    </TouchableOpacity>}
                    />
                </View>
            )
        }
        
    }
}