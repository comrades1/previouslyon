import React from 'react'
import { Text, View } from 'react-native';
import Series from './Series';
import Header from './Header'

export default class Home extends React.Component {
    
    render() {
        return (<View>
                    <Header/>
                    <Text>
                        Welcome {this.props.infos.login} !!
                    </Text>
                    <Series/>
                </View>)
    }
}